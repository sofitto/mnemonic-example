'use strict';
const seed = require('./bip39/seed');

const cardBackupKey = '38568E38DFC3F2903D0D91E0DBF79B46238FDBDAEA0845C34D7A0FECC7806BB4';
const backupKeyPhrase = seed.hexToSeedPhrase(cardBackupKey);
console.log('Backup key phrase: ' + backupKeyPhrase);

const expectedSeedPhrase = 'debate refuse mixed sauce dish elite vintage rate this text traffic middle decline unknown hill donate easy bounce future cabin small scan put list';
console.log('Reconstructed backup phrase equals expected Seed phrase?', backupKeyPhrase === expectedSeedPhrase);